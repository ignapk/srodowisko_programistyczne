#!/usr/bin/env bash

read -p "Podaj wymiar tabliczki mnożenia: " wymiar

for (( i=1; i<=$wymiar; i++ ))
do
	for (( j=1; j<=$wymiar; j++ ))
	do
		echo -n $((i * j))
	done
	echo
done
