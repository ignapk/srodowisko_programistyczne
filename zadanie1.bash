#!/usr/bin/env bash

read -p "Zarejestruj się/Zaloguj się - [1/2]: " answer

case $answer in
	1)
		read -p "Podaj nowy login: " login
		read -p "Podaj nowe haslo: " haslo
		encry=$( echo $haslo | base64 )
		tablica=($login $encry 0 0 0 0)
		echo ${tablica[@]} >> plik
		;;
	2)
		read -p "Podaj login: " login
		read -p "Podaj haslo: " password
		saved_login=$(grep "^$login" plik | cut -d' ' -f1)
		saved_password=$(grep "^$login" plik | cut -d' ' -f2 | base64 -d)
		if [ "$saved_login" == "$login" ] && [ "$saved_password" == "$password" ]
		then
			tablica=($(grep "^$login" plik))
			read -p "
			1 Dodaj dane
			2 Modyfikuj dane
			3 Usuń dane
			4 Wyświetl dane
			5 Zakończ skrypt
			[1/2/3/4/5/6/7]: " answer
			case $answer in
				1)
					read -p "
					1 Imie
					2 Nazwisko
					3 Adres e-mail
					4 wskazówka
					[1/2/3/4]: " answer
					case $answer in
						1)
							if [ ${tablica[2]} == "0" ]
							then
								read -p "Imie: " imie
								tablica[2]="$imie"
								sed -i -e "s/^$login.*/${tablica[*]}/" plik
							else
								echo "Juz jest"
							fi
							;;
						2)
							if [ ${tablica[3]} == "0" ]
							then
								read -p "Nazwisko: " nazwisko
								tablica[3]="$nazwisko"
								sed -i -e "s/^$login.*/${tablica[*]}/" plik
							else
								echo "Juz jest"
							fi
							;;
						3)
							if [ ${tablica[4]} == "0" ]
							then
								read -p "E-Mail: " email
								tablica[4]="$email"
								sed -i -e "s/^$login.*/${tablica[*]}/" plik
							else
								echo "Juz jest"
							fi
							;;
						4)
							if [ ${tablica[5]} == "0" ]
							then
								read -p "Wskazowka: " wskazowka
								tablica[5]="$wskazowka"
								sed -i -e "s/^$login.*/${tablica[*]}/" plik
							else
								echo "Juz jest"
							fi
							;;
						*)
							echo "Wybierz opcje 1-4"
							;;
					esac
					;;
				2)
					read -p "
					1 Imie
					2 Nazwisko
					3 Adres e-mail
					4 wskazówka
					[1/2/3/4]: " answer

					case $answer in
						1)
							read -p "Imie: " imie
							tablica[2]="$imie"
							sed -i -e "s/^$login.*/${tablica[*]}/" plik
							;;
						2)
							read -p "Nazwisko: " nazwisko
							tablica[3]="$nazwisko"
							sed -i -e "s/^$login.*/${tablica[*]}/" plik
							;;
						3)
							read -p "E-Mail: " email
							tablica[4]="$email"
							sed -i -e "s/^$login.*/${tablica[*]}/" plik
							;;
						4)
							read -p "Wskazowka: " wskazowka
							tablica[5]="$wskazowka"
							sed -i -e "s/^$login.*/${tablica[*]}/" plik
							;;
						*)
							echo "Wybierz opcje 1-4"
							;;
					esac
					;;
				3)
					echo "Usuwanie wszystkich danych"
					sed -ie "s/^$login.*//" plik 
					;;
				4)
					echo ${tablica[@]}
					;;
				5)
					exit
					;;
				*)
					echo "Wybierz opcje 1-5"
					;;
			esac
		else
			if [ "$saved_password" != "$password" ]
			then
				echo "Błędny login lub hasło"
			else
				echo "Użytkownik nie istnieje"
			fi
		fi
		;;
	*)
		echo "Wybierz opcję 1 lub 2"
		exit
		;;
esac
